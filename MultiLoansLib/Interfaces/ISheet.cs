﻿namespace MultiLoansLib.Interfaces
{
    using System;
    using System.Data;

    public interface ISheet
    {
        void Insert();
        DataTable SelectAllData(int sheetId);
    }
}

