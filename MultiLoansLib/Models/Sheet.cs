﻿namespace MultiLoansLib.Models
{
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class Sheet
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <CompanyType>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private string <Email>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private string <Firstname>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private string <Nip>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private string <Phone>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private string <Surname>k__BackingField;

        public int CompanyType { get; set; }

        public string Email { get; set; }

        public string Firstname { get; set; }

        public string Nip { get; set; }

        public string Phone { get; set; }

        public string Surname { get; set; }
    }
}

