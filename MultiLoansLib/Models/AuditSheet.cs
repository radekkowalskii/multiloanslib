﻿namespace MultiLoansLib.Models
{
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class AuditSheet : Sheet
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <CreditsCount>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private double <MonthlyCreditInstallment>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private double <TwoYearsNetProfit>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private double <TwoYearsSales>k__BackingField;

        public int CreditsCount { get; set; }

        public double MonthlyCreditInstallment { get; set; }

        public double TwoYearsNetProfit { get; set; }

        public double TwoYearsSales { get; set; }
    }
}

