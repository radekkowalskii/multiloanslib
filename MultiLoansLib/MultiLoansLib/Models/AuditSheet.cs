﻿namespace MultiLoansLib.Models
{
    using Npgsql;
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class AuditSheet : Sheet
    {
        private string _insertCommand = @"INSERT INTO unisheets (firstname, 
                                                                 surname, 
                                                                 email, 
                                                                 phone, 
                                                                 nip, 
                                                                 two_years_sales, 
                                                                 two_years_net_profit, 
                                                                 credits_count, 
                                                                 company_type, 
                                                                 monthly_credit_installment)
                                                         VALUES (@firstname, 
                                                                 @surname, 
                                                                 @email, 
                                                                 @phone, 
                                                                 @nip, 
                                                                 @two_years_sales, 
                                                                 @two_years_net_profit, 
                                                                 @credits_count, 
                                                                 @company_type, 
                                                                 @monthly_credit_installment)";
        protected override string InsertCommand => _insertCommand;

        public int CreditsCount { get; set; }
        public double MonthlyCreditInstallment { get; set; }
        public double TwoYearsNetProfit { get; set; }
        public double TwoYearsSales { get; set; }
        
        protected override List<NpgsqlParameter> GetInsertParams()
        {
            var parameters = new List<NpgsqlParameter>
            {
                new NpgsqlParameter("firstname", this.Firstname),
                new NpgsqlParameter("surname", this.Surname),
                new NpgsqlParameter("email", this.Email),
                new NpgsqlParameter("phone", this.Phone),
                new NpgsqlParameter("nip", this.Nip),
                new NpgsqlParameter("company_type", this.CompanyType),
                new NpgsqlParameter("two_years_sales", this.TwoYearsSales),
                new NpgsqlParameter("two_years_net_profit", this.TwoYearsNetProfit),
                new NpgsqlParameter("credits_count", this.CreditsCount),
                new NpgsqlParameter("monthly_credit_installment", this.MonthlyCreditInstallment)
            };
            return parameters;
        }
    }
}

