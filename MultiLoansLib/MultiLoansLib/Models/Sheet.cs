﻿namespace MultiLoansLib.Models
{
    using MultiLoansLib.Interfaces;
    using Npgsql;
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Threading.Tasks;

    public class Sheet : ISheet
    {
        private const string _insertCommand = @"INSERT INTO unisheets (firstname, 
                                                                       surname, 
                                                                       email, 
                                                                       phone, 
                                                                       nip, 
                                                                       two_years_sales, 
                                                                       two_years_net_profit, 
                                                                       credits_count, 
                                                                       company_type, 
                                                                       monthly_credit_installment
                                                               VALUES (@firstname, 
                                                                       @surname, 
                                                                       @email, 
                                                                       @phone, 
                                                                       @nip, 
                                                                       @two_years_sales, 
                                                                       @two_years_net_profit, 
                                                                       @credits_count, 
                                                                       @company_type, 
                                                                       @monthly_credit_installment)";
        protected virtual string InsertCommand => _insertCommand;
        private readonly string connectionString;
        public int CompanyType { get; set; }
        public string Email { get; set; }
        public string Firstname { get; set; }
        public string Nip { get; set; }
        public string Phone { get; set; }
        public string Surname { get; set; }
        
        public Sheet()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["multiloansConnectionString"].ConnectionString;
        }
        
        protected virtual List<NpgsqlParameter> GetInsertParams()
        {
            var parameters = new List<NpgsqlParameter>
            {
                new NpgsqlParameter("firstname", this.Firstname),
                new NpgsqlParameter("surname", this.Surname),
                new NpgsqlParameter("email", this.Email),
                new NpgsqlParameter("phone", this.Phone),
                new NpgsqlParameter("nip", this.Nip),
                new NpgsqlParameter("company_type", this.CompanyType)
            };
            return parameters;                
        }
        public void Insert()
        {
            using (NpgsqlConnection connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                using (NpgsqlCommand command = connection.CreateCommand())
                {
                    command.Parameters.AddRange(GetInsertParams().ToArray());
                    command.CommandText = InsertCommand;
                    command.ExecuteScalar();
                }
            }
        }

        public async Task InsertAsync()
        {
            using (NpgsqlConnection connection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["multiloansConnectionString"].ConnectionString))
            {
                await connection.OpenAsync();
                using (NpgsqlCommand command = connection.CreateCommand())
                {
                    command.Parameters.AddRange(GetInsertParams().ToArray());
                    command.CommandText = InsertCommand;
                    await command.ExecuteScalarAsync();
                }
            }
        }

        public virtual DataTable SelectAllData(int sheetId)
        {
            DataTable dataTable = new DataTable();
            using (NpgsqlConnection connection = new NpgsqlConnection(connectionString))
            {
                connection.OpenAsync();
                using (NpgsqlCommand command = connection.CreateCommand())
                {
                    command.Parameters.AddWithValue("id", sheetId);
                    command.CommandText = "SELECT * FROM unisheets WHERE id = @id";
                    using (NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter(command))
                    {
                        dataAdapter.Fill(dataTable);
                    }
                }
            }
            return dataTable;
        }
    }
}

