﻿namespace MultiLoansLib.Interfaces
{
    using System;
    using System.Data;
    using System.Threading.Tasks;

    public interface ISheet
    {
        void Insert();
        Task InsertAsync();
        DataTable SelectAllData(int sheetId);
    }
}

